import json
import boto3
import uuid

def lambda_handler(event, context):
    # TODO implement
    # print(event)
    filename = event['ip'].split('.')
    filename = '-'.join(filename)
    myuuid = filename
    config = f"""[version]
    format=1.0
    [connect]
    host={event['ip']}
    port=8443
    weburlpath=
    sessionid=
    proxyport=0
    proxytype=system"""

    # Method 1: Object.put()
    s3 = boto3.resource('s3')
    object = s3.Object('dcvfiles', f'{myuuid}.dcv')
    object.put(Body=config)
    return {
        'statusCode': 200,
        'body': json.dumps('fileWritten')
    }
