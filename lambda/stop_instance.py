import boto3

def lambda_handler(event, context):
    ec2 = boto3.client("ec2")
    ec2.stop_instances(InstanceIds=[event["instanceId"]])
    #ec2.terminate_instances(InstanceIds=[event["instanceId"]])

    cwe = boto3.client("events")
    cwe.remove_targets(Rule=event["instanceId"],Ids=["stopEC2instance"])
    cwe.delete_rule(Name=event["instanceId"])
    
    lbd = boto3.client("lambda")
    #replace the FunctionName with your own Functionname
    lbd.remove_permission(FunctionName='StopE2Instance',StatementId='SID'+event["instanceId"])
