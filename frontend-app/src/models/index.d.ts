import { ModelInit, MutableModel, PersistentModelConstructor } from "@aws-amplify/datastore";





export declare class Account {
  readonly id: string;
  readonly Email?: string;
  readonly Given_Name?: string;
  readonly Last_Name?: string;
  readonly Password?: string;
  constructor(init: ModelInit<Account>);
  static copyOf(source: Account, mutator: (draft: MutableModel<Account>) => MutableModel<Account> | void): Account;
}