// AWS Imports
import Amplify from 'aws-amplify'
import awsconfig from './components/Information/aws-exports'

// Third party libraries
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

// Local files
import App from './App';
import * as serviceWorker from './serviceWorker';
import Tutorial from './components/Tutorial';
import ContactUs from './components/ContactUs';

// important: For amplify to take in aws-exports.js data
Amplify.configure(awsconfig)

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <Switch>
        <Route exact path="/">
          <App />
        </Route>
        <Route path="/tutorial">
          <Tutorial />
        </Route>
        <Route path="/contact">
          <ContactUs />
        </Route>
      </Switch>
    </Router>
  </React.StrictMode >,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
