// AWS Imports
import { Auth } from 'aws-amplify';
import { ConfirmSignIn, RequireNewPassword, VerifyContact, withAuthenticator } from 'aws-amplify-react'
import CustomSignIn from './components/Auth/SignIn';
import CustomSignUp from './components/Auth/SignUp';
import CustomConfirmation from './components/Auth/Confirmation';
import CustomForgotPassword from './components/Auth/ForgotPassword';

// Third party libraries
import React, { useState, useEffect } from "react";
import { BrowserRouter as Route } from "react-router-dom";

// Local files
import MainPage from './MainPage.jsx';

// Base app   
function App() {
  const [loggedIn, setLoggedIn] = useState(false);

  const assessLoggedInState = () => {
    Auth.currentAuthenticatedUser()
      .then(sess => {
        // console.log('Logged in');
        setLoggedIn(true);
      })
      .catch(() => {
        // console.log('Not logged in');
        setLoggedIn(false);
      })
  };

  useEffect(() => {
    assessLoggedInState();
  }, []);

  const signOut = async () => {
    try {
      await Auth.signOut();
      setLoggedIn(false);
    } catch (err) {
      // console.log('Error signing out ', err)
    }
  }

  return (
    <div className="App">
      <Route exact path="/">
        <MainPage authState={loggedIn} signOut={signOut} />
      </Route>
    </div>
  );
}

// Authenticator
export default withAuthenticator(App, false, [
  <CustomSignIn />,
  <ConfirmSignIn />, // not needed since no MFA
  <VerifyContact />, // WIP (Not needed in current Auth flow)
  <CustomSignUp />,
  <CustomConfirmation />,
  <CustomForgotPassword />,
  <RequireNewPassword /> // WIP
]);