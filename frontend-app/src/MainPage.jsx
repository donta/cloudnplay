import React, { useState, useEffect } from 'react';
import './css/MainPage.scss';
import awsmobile from './components/Information/aws-exports.js';
import AWS from 'aws-sdk';
import '@aws-amplify/ui/dist/style.css';
import 'bootstrap/dist/css/bootstrap.css';

// import components
import { GameDeck, TopNav, StartDiv, TutorialDiv, Countdown, UserPool, ContactDiv } from './components/Information'

const MainPage = (props) => {
  //state construction
  const [UserState, SetUserState] = useState(
    {
      user: null,
      cwe: null,
      lambda: null,
      ec2: null,
      instance: null,
      status: null,
      start_time: null,
      ip: null,
      No_instance: null
    }
  )

  // Timer function
  const childRef = React.createRef();

  const triggerChildMethod = () => {
    childRef.current.startTimer();
  }


  const [EventFlag, SetEventFlag] = useState(
    'firstload'
  )

  const [GetInstanceFlag, SetGetInstanceFlag] = useState(
    { No: 0 }
  )

  useEffect(() => {
    // console.log(UserState)
  })

  useEffect(() => {
    if (EventFlag === 'firstload') {
      // console.log('first')
      getUserAndAwsService()
      // console.log(UserState)
    }
    else if (EventFlag === 'SecondFlag') {
      // console.log('second')
      getInstanceDetails();
      // console.log(UserState)
    }
    else if (EventFlag === 'ThirdFlag') {
      if (UserState.status !== null) {
        // console.log('third')
        // V (Disabled so user need to manually click on any button to start playing) V
        // startPlaying(GetInstanceFlag.boolNiceDCV)
        // console.log(UserState)
      }
    }
    else if (EventFlag === 'FourthFlag') {
      // console.log('fourth')
      clearScheduledStop(UserState.instance);
      updateTag([UserState.instance], [{ Key: "Player", Value: UserState.user }]);
      startPlaying(GetInstanceFlag.boolNiceDCV)
      // console.log(UserState)
    }
    else if (EventFlag === 'FifthFlag') {
      // console.log('last')
      updateTag([UserState.instance], [{ Key: "Player", Value: UserState.user }]);
      // console.log(UserState)
    }
  }, [EventFlag]) // eslint-disable-line react-hooks/exhaustive-deps

  const getUserAndAwsService = () => {
    // var userPool = new CognitoUserPool(poolData)
    var cognitoUser = UserPool.getCurrentUser();

    if (cognitoUser != null) {
      cognitoUser.getSession(function (err, result) {
        if (result) {
          // console.log('You are now logged in.');
          // console.log(result)
          // console.log(result.getIdToken())
          // console.log(result.getIdToken().getJwtToken())

          // Add the User's Id Token to the Cognito credentials login map
          AWS.config.credentials = new AWS.CognitoIdentityCredentials({
            IdentityPoolId: awsmobile["aws_cognito_identity_pool_id"],
            Logins: {
              // Change the key below with your region and user pool id
              'cognito-idp.ap-southeast-1.amazonaws.com/ap-southeast-1_ImnJfJVMR': result.getIdToken().getJwtToken()
            }
          }, {
            region: awsmobile["aws_project_region"]
          });

          AWS.config.credentials.refresh(function (err) {
            if (err) {
              // console.log(err);
            }
          });

          // console.log(AWS.config.credentials)
        }
      });

      SetUserState({
        user: cognitoUser.getUsername(),
        cwe: new AWS.CloudWatchEvents({
          credentials: AWS.config.credentials,
          region: awsmobile["aws_project_region"]
        }),
        lambda: new AWS.Lambda({
          credentials: AWS.config.credentials,
          region: awsmobile["aws_project_region"]
        }),
        ec2: new AWS.EC2({
          credentials: AWS.config.credentials,
          region: awsmobile["aws_project_region"]
        })
      });

      SetEventFlag('SecondFlag')

    }
  }


  const getInstanceDetails = () => {
    var params = {
      Filters: [
        {
          Name: "tag:Player",
          Values: [UserState.user]
        }
      ]
    };

    var status = null;
    var id = null;
    var start_time = null;
    var ip = null;
    var No_instance = null;

    UserState.ec2.describeInstances(params, (err, data) => {
      if (err) {
        // console.log(err);
      }
      else if (data.Reservations[0]) {
        // console.log(data);
        var instance = data.Reservations[0].Instances[0]
        id = instance.InstanceId;
        status = instance.State.Name;
        start_time = instance.LaunchTime;
        if (status === "running") {
          ip = instance.PublicIpAddress;
        }
      }

      SetUserState({
        user: UserState.user,
        cwe: UserState.cwe,
        lambda: UserState.lambda,
        ec2: UserState.ec2,
        instance: id,
        status: status,
        start_time: start_time,
        ip: ip,
        NoInstance: No_instance
      });

      SetEventFlag('ThirdFlag')


    });

  }

  // const AMI = ''

  useEffect(() => {
    // If statement to prevent initial trigger of function.
    if (GetInstanceFlag.No !== 0) {
      if (GetInstanceFlag.AMI === 'NO_AMI') {
        alert('No Image Loaded. Please inform the adminstrator to fix it')
        return
      }

      if (UserState.instance !== null) {
        startPlaying(false)
        // To end the function should an instance is already tagged to the account
        return
      }

      //Else, find instance
      findRunningInstance(GetInstanceFlag)
      //if free instance is found and successfully tagged

    }
  }, [GetInstanceFlag]) // eslint-disable-line react-hooks/exhaustive-deps

  const GetInstance = (gamedetails, boolNiceDCV) => {
    // Number incremental to ensure that changes will always be detected by the useEffect above 
    SetGetInstanceFlag({
      No: GetInstanceFlag.No + 1,
      Name: gamedetails.name,
      AMI: gamedetails.AMI,
      boolNiceDCV: boolNiceDCV
    })
  }

  const findRunningInstance = (gamedetails) => {
    var id;
    var params = {
      Filters: [
        {
          Name: "tag:Player",
          Values: [""]
        },
        {
          Name: "instance-state-name",
          Values: ["running"]
        },
        {
          Name: "tag:AMI",
          Values: [gamedetails.AMI]
        }
      ]
    };
    // console.log(UserState)
    UserState.ec2.describeInstances(params, (err, data) => {
      if (err) {
        // console.log("Describe Instance Error: " + err);
      }
      else if (data.Reservations[0]) {
        // console.log(data);
        var result = data.Reservations;
        var max = 0;
        for (var i = 0; i < result.length; i++) {
          var extra_time = 3600000 - ((new Date() - result[i].Instances[0].LaunchTime) % 3600000);
          if (extra_time > max) {
            max = extra_time;
            id = result[i].Instances[0].InstanceId;
          }
        }
        SetUserState({
          user: UserState.user,
          cwe: UserState.cwe,
          lambda: UserState.lambda,
          ec2: UserState.ec2,
          instance: id,
          status: UserState.status,
          start_time: UserState.start_time,
          ip: UserState.ip,
          NoInstance: UserState.No_instance
        })

        SetEventFlag('FourthFlag')

      }
      else {
        findStoppedInstance(gamedetails);

      }
    });
  }

  const findStoppedInstance = (gamedetails) => {
    var params = {
      Filters: [
        {
          Name: "tag:Player",
          Values: [""]
        },
        {
          Name: "instance-state-name",
          Values: ["stopped"]
        },
        {
          Name: "tag:AMI",
          Values: [gamedetails.AMI]
        }
      ]
    };

    UserState.ec2.describeInstances(params, (err, data) => {
      // // console.log(this.state.ec2)
      // // console.log(err)
      // // console.log(data)
      // // console.log(params)
      if (err) {
        // console.log("Describe Instance Error: " + err);
      }
      else if (data.Reservations[0]) {
        // // console.log(data);
        SetUserState({
          user: UserState.user,
          cwe: UserState.cwe,
          lambda: UserState.lambda,
          ec2: UserState.ec2,
          instance: data.Reservations[0].Instances[0].InstanceId,
          status: UserState.status,
          start_time: UserState.start_time,
          ip: UserState.ip,
          NoInstance: UserState.No_instance
        })

        SetEventFlag('FifthFlag')


        startPlaying(GetInstanceFlag.boolNiceDCV)
      }
      else {
        // alert("No instance available at the moment!")
        launchinstance(gamedetails)
        alert('Instance launched. Please refresh your screen after 3 mins and press the button again')
      }
    });
  }

  const startPlaying = () => {
    if (UserState.status === "running") {
      if (GetInstanceFlag.boolNiceDCV === true) {
        openConnection();
      }
      else {
        openConnection_tab();
      }
    }
    else if (UserState.status === "stopped") {
      startInstance();
    }
  }

  const startInstance = () => {
    var params = {
      InstanceIds: [UserState.instance]
    };

    alert("Starting instance...")

    var starttime = new Date();

    UserState.ec2.startInstances(params, (err, data) => {
      if (err) {
        // console.log("Start Instance Error: " + err);
      }
      else {
        // console.log(data);
        var interval = setInterval(() => {
          getInstanceDetails();
          if (UserState.status === "running") {
            clearInterval(interval);
            var endtime = new Date();
            var startingtime = endtime - starttime;
            // console.log("Starting time: " + startingtime);
            openConnection();
          }
        }, 1000);
      }
    });
  }

  const downloadBlob = (blob, fileName) => {
    // Convert your blob into a Blob URL (a special url that points to an object in the browser's memory)
    const blobUrl = URL.createObjectURL(blob);
    // console.log("!!!!!!!!!!!!!!!!!!!!")
    // console.log(blobUrl)
    // console.log(fileName)
    // Create a link element
    const link = document.createElement('a');
    // Set link's href to point to the Blob URL
    link.href = blobUrl;
    link.download = fileName;
    // Append link to the body
    document.body.appendChild(link);
    // Dispatch click event on the link
    // This is necessary as link.click() does not work on the latest firefox
    link.dispatchEvent(
      new MouseEvent('click', {
        bubbles: true,
        cancelable: true,
        view: window,
      })
    );
    // Remove link from body
    document.body.removeChild(link);
  }

  const openConnection = () => {
    // console.log(UserState);
    var params = {
      //change the function name
      FunctionName: "CreateDCVFile-staging",
      Payload: `{"ip": "${UserState.ip}"}`,
    };
    // console.log(UserState.lambda)
    UserState.lambda.invoke(params, function (err, data) {
      if (err) console.log(err, err.stack); // an error occurred
      else {
        let ipadd = UserState.ip.toString();
        let ipadd_list = ipadd.split(".");
        let keyName = ipadd_list.join("-") + ".dcv";
        // console.log(keyName);

        var s3 = new AWS.S3({
          //change the keys
          accessKeyId: '<input ur accesskey>',
          secretAccessKey: '<input ur secretAccessKey>'
        });
        // this part might need to be a function as different game will call different ip address/files to be downloaded
        s3.getObject(
          { Bucket: "dcvfiles", Key: keyName },
          function (error, data) {
            if (error != null) {
              alert("Failed to retrieve an object: " + error);
            } else {
              alert("Loaded " + data.ContentLength + " bytes");
              // console.log(data);
              let csvBlob = new Blob([data.Body.toString()], {
                type: 'text/csv;charset=utf-8;',
              });
              downloadBlob(csvBlob, keyName);
            }
          });
      }
    });
  }

  const openConnection_tab = () => {
    var new_address = "https://" + UserState.ip + ":8443/#console";
    window.open(new_address, "_blank");
    window.focus();
    alert("Connection opened! Press \"Play\" on Parsec to start");
  }

  const optimizeUsage = () => {
    var extra_time = 3420000 - ((new Date() - UserState.start_time) % 3600000);

    if (UserState.status === "running") {
      if (extra_time < 120000) {
        stopInstance();
      }
      else {
        scheduleStop(extra_time);
      }
    }
    else {
      updateTag([UserState.instance], [{ Key: "Player", Value: "" }]);
    }

    alert("Instance released!");
  }

  const scheduleStop = (extra_time) => {
    var sched_time = new Date();
    sched_time.setMilliseconds(sched_time.getMilliseconds() + extra_time);
    var cron = ('cron(' + sched_time.getUTCMinutes() + ' ' + sched_time.getUTCHours() + '/1 * * ? *)');
    // console.log(cron);
    var params = {
      Name: UserState.instance,
      ScheduleExpression: cron
    };

    UserState.cwe.putRule(params, (err, data) => {
      if (err) {
        // console.log("Put Rule Error: " + err);
      }
      else {
        // console.log(data);
        var ruleArn = data.RuleArn;
        var permission_params = {
          Action: 'lambda:InvokeFunction',
          // Change with your Lambda function name
          FunctionName: "StopE2Instance",
          StatementId: ('SID' + UserState.instance),
          Principal: 'events.amazonaws.com',
          SourceArn: ruleArn
        };
        UserState.lambda.addPermission(permission_params, (err, data) => {
          // console.log(permission_params);
          // console.log(err);
          // console.log(data);
          if (err) {
            // console.log("Add Permission Error: " + err);
          }
          else {
            // console.log(data);
            var input = "{\"instanceId\" : \"" + UserState.instance + "\"}";
            var target_params = {
              Rule: UserState.instance,
              Targets: [{
                // Change with your Lambda Arn
                Arn: 'arn:aws:lambda:ap-southeast-1:079212341369:function:StopE2Instance',
                Id: 'stopEC2instance',
                Input: input
              }]
            };
            UserState.cwe.putTargets(target_params, (err, data) => {
              if (err) {
                // console.log("Put Target Error: " + err);
              }
              else {
                // console.log(data);
                updateTag([UserState.instance], [{ Key: "Player", Value: "" }]);
              }
            });
          }
        });
      }
    });
  }

  const clearScheduledStop = (id) => {
    var params = {
      // Change with your Lambda function name
      FunctionName: "StopE2Instance-staging",
      StatementId: "SID" + id
    };

    UserState.lambda.removePermission(params, (err, data) => {
      if (err) {
        // console.log("Remove Permission Error: " + err);
      }
      else {
        // console.log(data);
        var target_params = {
          Rule: id,
          Ids: ["stopEC2instance"]
        };
        UserState.cwe.removeTargets(target_params, (err, data) => {
          if (err) {
            // console.log("Remove Target Error: " + err);
          }
          else {
            // console.log(data);
            var rule_params = {
              Name: id
            };
            UserState.cwe.deleteRule(rule_params, (err, data) => {
              if (err) {
                // console.log("Delete Rule Error: " + err);
              }
              else {
                // console.log(data);
              }
            });
          }
        });
      }
    });
  }

  const stopInstance = () => {
    var params = {
      InstanceIds: [UserState.instance]
    };

    UserState.ec2.terminateInstances(params, (err, data) => {
      if (err) {
        // console.log("Stop Instance Error: " + err);
      }
      else {
        // console.log(data);
        var interval = setInterval(() => {
          getInstanceDetails();
          if (UserState.status === "stopped") {
            clearInterval(interval);
          }
        }, 1000);
        updateTag([UserState.instance], [{ Key: "Player", Value: "" }]);
      }
    });
  }

  const updateTag = (instances, tags) => {
    var params = {
      Resources: instances,
      Tags: tags
    };

    UserState.ec2.createTags(params, (err, data) => {
      if (err) {
        // console.log("Update Tag Error: " + err);
      }
      else {
        // console.log(data);
        getInstanceDetails();
      }
    });
  }


  //Start of creating a new instance
  const launchinstance = (gamedetails) => {
    // console.log(UserState)
    let playeruser = UserState.user
    AWS.config.update({ region: 'ap-southeast-1' });

    // Start the timer going --> for now using timer
    triggerChildMethod();

    // AMI is amzn-ami-2011.09.1.x86_64-ebs
    var launchtemplate = {
      // Change the LaunchtemplateID
      LaunchTemplateId: 'lt-04ba5b6b9e7785aa5',
      Version: '4'
    }
    // console.log(gamedetails)
    var instanceParams = {
      LaunchTemplate: launchtemplate,
      ImageId: gamedetails.AMI,
      MinCount: 1,
      MaxCount: 1,

    };

    // Create a promise on an EC2 service object
    var instancePromise = new AWS.EC2({ apiVersion: '2016-11-15' }).runInstances(instanceParams).promise();
    var starttime = new Date();
    // Handle promise's fulfilled/rejected states
    instancePromise.then(
      function (data) {
        // console.log(playeruser);
        var instanceId = data.Instances[0].InstanceId;
        // console.log("Created instance", instanceId);
        // Add tags to the instance
        let tagParams = {
          Resources: [instanceId], Tags: [
            {
              Key: 'Name',
              Value: gamedetails.Name,
            },
            {
              Key: 'Player',
              Value: playeruser,
            },
            {
              Key: 'AMI',
              Value: gamedetails.AMI,
            }
          ]
        };
        // Create a promise on an EC2 service object
        var tagPromise = new AWS.EC2({ apiVersion: '2016-11-15' }).createTags(tagParams).promise();
        // Handle promise's fulfilled/rejected states
        tagPromise.then(
          function (data) {
            // console.log("Instance tagged");
          }).catch(
            function (err) {
              console.error(err, err.stack);
            });

        //start the interval countdown

        var interval = setInterval(() => {
          clearInterval(interval);
          var endtime = new Date();
          var startingtime = endtime - starttime;
          // console.log("Starting time: " + startingtime);
        })
      }).catch(
        function (err) {
          console.error(err, err.stack);
        });
  }


  return (
    <div className="App">
      <div id="topdiv">
        <TopNav />
        {props.authState ? <button id="mainpage-button" onClick={props.signOut}>Logout</button> : <div></div>}
      </div>
      <StartDiv />
      <h2 className="Heading" id="games">Available Games</h2>
      <div id="games-div">
        <p style={{ fontSize: "12px", opacity: '50%' }}>Password: <b>Cloudis10%fluffy</b></p>
        <button className="release-button" disabled={UserState.instance === null} onClick={optimizeUsage}>Release Instance</button>
        <Countdown ref={childRef} />
        <GameDeck AmiLaunch={GetInstance} />
      </div>
      <h2 className="Heading" id="main-tutorial">Tutorial</h2>
      <div className="Group">
        <TutorialDiv />
      </div>
      <div>
        <h2 className="Heading" id="contact">Contact Us</h2>
        <ContactDiv />
      </div>
    </div>
  )

}

export default MainPage