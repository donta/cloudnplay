// AWS Imports
import { Auth } from "aws-amplify";
import { SignUp } from "aws-amplify-react";

// Third party libraries
import React from "react";
import { Form, Button, Card } from 'react-bootstrap';

// Local files
import AuthNav from "./AuthNav";
import '../../css/forms.scss';

class CustomSignUp extends SignUp {
    constructor(props) {
        super(props);
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleFormSubmission = this.handleFormSubmission.bind(this)
        this.validateForm = this.validateForm.bind(this)
        this.state = {
            error: '',
        }
    }

    validateForm() {
        if (this.inputs.username === undefined || this.inputs.password === undefined || this.inputs.email === undefined) {
            return false;
        }
        return this.inputs.username.length > 0 && this.inputs.password.length > 0 && this.inputs.email.length > 0;
    }

    handleFormSubmission(evt) {
        evt.preventDefault();
        this.signUp();
    }

    async signUp() {
        const username = this.inputs.username
        const password = this.inputs.password
        const email = this.inputs.email
        try {
            await Auth.signUp({
                username,
                password,
                attributes: {
                    email, // optional
                    // phone_number, // optional - E.164 number convention
                    // other custom atttributes (For the future)
                }
            })
            alert('Registered successfully');
            super.changeState('confirmSignUp');
        } catch (err) {
            if (username === undefined || password === undefined || email === undefined) {
                this.setState({ error: 'Make sure fields are filled' });
                alert('Make sure fields are filled');
            }
            else if (username === "" || password === "") {
                this.setState({ error: 'Make sure fields are filled' });
                alert('Make sure fields are filled');
            }
            else {
                this.setState({ error: 'An error has occurred.' })
                alert('An error has occurred.')
            }
        }
    }

    handleInputChange(evt) {
        this.inputs = this.inputs || {}
        const { name, value, type, checked } = evt.target
        const check_type = ['radio', 'checkbox'].includes(type)
        this.inputs[name] = check_type ? checked : value
        this.inputs['checkedValue'] = check_type ? value : null
        this.setState({ error: '' })
    }

    showComponent(theme) {
        return (
            <div className="signup">
                <div className="container">
                    <div className="row">
                        <AuthNav
                            status={"register"}
                            signIn={() => super.changeState('signIn')}
                            signUp={() => super.changeState('signUp')}
                        />
                    </div>
                    <div className="row">
                        <Card>
                            <Card.Body>
                                <Card.Title>Create your account</Card.Title>
                                <Form className="signin-form" onSubmit={this.handleFormSubmission}>
                                    <Form.Group size="lg" controlId="username">
                                        <Form.Label>Username</Form.Label>
                                        <Form.Control
                                            autoFocus
                                            key="username"
                                            name="username"
                                            type="text"
                                            placeholder="Username"
                                            onChange={this.handleInputChange}
                                        />
                                    </Form.Group>
                                    <Form.Group size="lg" controlId="password">
                                        <Form.Label>Password</Form.Label>
                                        <Form.Control
                                            key="password"
                                            name="password"
                                            type="password"
                                            placeholder="Password"
                                            onChange={this.handleInputChange}
                                        />
                                    </Form.Group>
                                    <Form.Group size="lg" controlId="email">
                                        <Form.Label>Email</Form.Label>
                                        <Form.Control
                                            key="email"
                                            name="email"
                                            type="email"
                                            placeholder="Hello@gmail.com"
                                            onChange={this.handleInputChange}
                                        />
                                    </Form.Group>
                                    <Button className="signin-button" block size="lg" type="submit" value="Sign Up" disabled={!this.validateForm()}>
                                        SIGN UP
                                    </Button>
                                </Form>
                                {/* To change the onClick at later time */}
                                <Card.Link href="#" onClick={() => super.changeState('signIn')}>Back to Login page</Card.Link>
                            </Card.Body>
                        </Card>
                    </div>
                </div>
            </div>
        )
    }
}

export default CustomSignUp;