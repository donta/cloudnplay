// AWS Imports
import { ConfirmSignUp } from "aws-amplify-react";

// Third party libraries
import React from "react";
import { Card } from 'react-bootstrap';

// Local files
import AuthNav from "./AuthNav";
import '../../css/confirmation.scss'

class CustomConfirmation extends ConfirmSignUp {
  constructor(props) {
    super(props);
    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleFormSubmission = this.handleFormSubmission.bind(this)
    this.state = {
      error: '',
    }
  }

  handleFormSubmission(evt) {
    evt.preventDefault();
  }

  showComponent(theme) {
    return (
      <div className="confirmation">
        <div className="container">
          <div className="row">
            <AuthNav
              signIn={() => super.changeState('signIn')}
              signUp={() => super.changeState('signUp')}
            />
          </div>
          <div className="row">
            <Card>
              <Card.Body>
                <Card.Title>Your account has been successfully created!</Card.Title>
                {/* WIP - Sending of email doesn't work */}
                <Card.Text>Please wait for Administrator’s approval. We will send you an email once we have approved your access.</Card.Text>
                <Card.Link href="#" onClick={() => super.changeState('signIn')}>Back to Login page</Card.Link>
              </Card.Body>
            </Card>
          </div>
        </div>
      </div>
    )
  }
}

export default CustomConfirmation;