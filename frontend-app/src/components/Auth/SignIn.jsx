// AWS Imports
import { Auth } from 'aws-amplify'
import { SignIn } from 'aws-amplify-react';

// Third party libraries
import React from 'react';
import { Card, Form, Button } from 'react-bootstrap';

// Local files
import AuthNav from './AuthNav';
import '../../css/forms.scss';

class CustomSignIn extends SignIn {
  constructor(props) {
    super(props);
    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleFormSubmission = this.handleFormSubmission.bind(this)
    this.validateForm = this.validateForm.bind(this)
    this.state = {
      error: ''
    }
  }

  validateForm() {
    if (this.inputs.username === undefined || this.inputs.password === undefined) {
      return false;
    }

    return this.inputs.username.length > 0 && this.inputs.password.length > 0;
  }

  handleFormSubmission(evt) {
    evt.preventDefault();
    this.signIn();
  }

  async signIn() {
    const username = this.inputs.username
    const password = this.inputs.password
    try {
      await Auth.signIn(username, password)
      this.props.onStateChange('signedIn', {})
    } catch (err) {
      if (username === undefined || password === undefined) {
        this.setState({ error: 'Make sure fields are filled' });
        alert('Make sure fields are filled');
      }
      else if (username === "" || password === "") {
        this.setState({ error: 'Make sure fields are filled' });
        alert('Make sure fields are filled');
      }
      else if (err.code === 'UserNotConfirmedException') {
        super.changeState('confirmSignUp');
      }
      else if (err.code === 'NotAuthorizedException') {
        // The error happens when the incorrect password is provided
        this.setState({ error: 'Login failed.' });
        alert('Login failed.')
      }
      else if (err.code === 'UserNotFoundException') {
        // The error happens when the supplied username/email does not exist in the Cognito user pool
        this.setState({ error: 'Login failed.' })
        alert('Login failed.')
      }
      else {
        this.setState({ error: 'An error has occurred.' })
        alert('An error has occurred. Please check with codebase or AWS')
      }
    }
  }

  handleInputChange(evt) {
    this.inputs = this.inputs || {}
    const { name, value, type, checked } = evt.target
    const check_type = ['radio', 'checkbox'].includes(type)
    this.inputs[name] = check_type ? checked : value
    this.inputs['checkedValue'] = check_type ? value : null
    this.setState({ error: '' })
  }

  showComponent(theme) {
    return (
      <div className="signin">
        <div className="container">
          <div className="row">
            <AuthNav
              status={"login"}
              signIn={() => super.changeState('signIn')}
              signUp={() => super.changeState('signUp')}
            />
          </div>
          <div className="row">
            <Card>
              <Card.Body>
                <Card.Title>Login to your account</Card.Title>
                <Form className="signin-form" onSubmit={this.handleFormSubmission}>
                  <Form.Group size="lg" controlId="username">
                    <Form.Label>Username</Form.Label>
                    <Form.Control
                      autoFocus
                      key="username"
                      name="username"
                      type="text"
                      placeholder="Username"
                      onChange={this.handleInputChange}
                    />
                  </Form.Group>
                  <Form.Group size="lg" controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                      key="password"
                      name="password"
                      type="password"
                      placeholder="Password"
                      onChange={this.handleInputChange}
                    />
                  </Form.Group>
                  <Button className="signin-button" block size="lg" type="submit" value="Sign In" disabled={!this.validateForm()}>
                    SIGN IN
                  </Button>
                </Form>
                <Card.Link href="#" onClick={() => super.changeState('forgotPassword')}>Reset your password</Card.Link>
              </Card.Body>
            </Card>
          </div>
        </div>
      </div>
    )
  }
}

export default CustomSignIn;