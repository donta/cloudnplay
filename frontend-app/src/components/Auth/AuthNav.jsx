// AWS Imports

// Third party libraries
import React from "react";

// Local files
import '../../css/authnav.scss';


function AuthNav(props) {
  return (
    <div className="auth-nav">
      {/* To change in the future */}
      <img className="nav-img"
        src="/images/logo_transparent.png"
        alt="logo"
      />
      <div className="auth-nav-right">
        {props.status === "login" ? <button className="auth-nav-tab" onClick={props.signUp}>Register</button> : <a className="auth-nav-tab" href="/" onClick={props.signIn}>Login</a>}
        <a className="auth-nav-tab" href="/tutorial">Tutorial</a>
        <a className="auth-nav-tab" href="/contact">Contact Us</a>
      </div>
    </div>
  )
}

export default AuthNav;