// AWS Imports
import { ForgotPassword } from 'aws-amplify-react';

// Third party libraries
import React from 'react';
import { Card } from 'react-bootstrap';

// Local files
import AuthNav from './AuthNav';
import '../../css/forms.scss';


class CustomForgotPassword extends ForgotPassword {

  showComponent(theme) {
    return (
      <div className="signin">
        <div className="container">
          <div className="row">
            <AuthNav
              signIn={() => super.changeState('signIn')}
              signUp={() => super.changeState('signUp')}
            />
          </div>
          <div className="row">
            <Card>
              <Card.Body>
                <Card.Title>Reset your password</Card.Title>
                {/* Forgot Password Email - WIP */}
                <Card.Text className="my-5">
                  We've sent an email alerting the team regarding resetting of your account's password. Once received, the team will contact you via phone call.
                </Card.Text>
                <Card.Link href="#" onClick={() => super.changeState('signIn')}>Back to Login page</Card.Link>
              </Card.Body>
            </Card>
          </div>
        </div>
      </div>
    )
  }


}

export default CustomForgotPassword;