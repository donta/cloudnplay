import React from 'react';

const TopNav = () => {
    return (
        <div className="navbar-custom" id="home">
            <a className="nav-tab" href="#home">Home</a>
            <a className="nav-tab" href="#games">Games</a>
            <a className="nav-tab" href="#main-tutorial">Tutorial</a>
            <a className="nav-tab" href="#contact">Contact Us</a>
        </div>
    );
};

export default TopNav;