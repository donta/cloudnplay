//Internal 
import {GamesDatabase, GameCard} from './Information'

// Modules
import React, { useState } from 'react';

const GameDeck = (AmiLaunch) => {
    // eslint-disable-next-line
    const [ gamesList, setGamesList ] = useState(GamesDatabase);
    // console.log(AmiLaunch)
    
    
    return (
        <div className="row-custom">
            {gamesList.map(game => {
                return (
                    <GameCard game={game} key={game.id + game.name} AmiLaunch = {AmiLaunch.AmiLaunch} />
                )
            })}
        </div>
    );
};

export default GameDeck;

