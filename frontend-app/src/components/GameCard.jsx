import React from 'react';

const GameCard = ({game, AmiLaunch}) => {
    let source = '/images/' + game.gameImg;

    return (
        <div className="card-div" >
            <div className="card-custom"> 
                <img src={source} alt={game.name}/>
            </div>
            <div className="mainpage-buttons">
                <button className='mainpage-button' onClick= {() => AmiLaunch(game, false)}>Parsec</button>
                <button className='mainpage-button' onClick= {() => AmiLaunch(game, true)}>Nice DCV</button>
            </div>
            
        </div>
            
    );
};

export default GameCard;