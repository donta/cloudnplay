// AWS Imports

// Third party libraries
import React from "react";
import { Navbar } from 'react-bootstrap';
import { withRouter } from 'react-router';

// Local files

function Navigation(props) {
  return (
    <nav className="navbar navbar-expand navbar-dark bg-dark">
      <div className="container-fluid">
        <Navbar bg="dark" variant="dark">
          <Navbar.Brand href="#home">
            <img
              src="/images/logo_transparent.png"
              width="70"
              height="auto"
              className="d-inline-block align-top"
              alt='Logo'
            />
          </Navbar.Brand>
        </Navbar>
        {props.authState ? <button onClick={props.signOut}>Logout</button> : <div></div>}
      </div>
    </nav>
  );
}

export default withRouter(Navigation);