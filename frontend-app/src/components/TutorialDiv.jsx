import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';

const TutorialDiv = () => {
    return (
            <iframe className="tutorial-video" width="720" height="480" src="https://www.youtube.com/embed/SzPhx5GGay8" title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
    )
}

export default TutorialDiv;