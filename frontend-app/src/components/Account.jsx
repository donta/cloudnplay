// Internal
import {UserPool} from "./Information";

// Modules
import React, { createContext } from "react";
import { useHistory } from "react-router-dom";
import * as AWSCognito from 'amazon-cognito-identity-js';

const AccountContext = createContext();

const Account = (props) => {

    const getSession = async () => {
        return await new Promise((resolve, reject) => {
            const user = UserPool.getCurrentUser();
            if (user) {
                user.getSession((err, session) => {
                    if (err) {
                        reject();
                    } else {
                        resolve(session);
                    }
                });
            } else {
                reject();
            }
        });
    };


    const authenticate = async (Username, Password) => {
        await new Promise((resolve, reject) => {
            var userData = {
                Username,
                Pool: UserPool
            };

            var cognitoUser = new AWSCognito.CognitoUser(userData);

            var authenticationData = {
                Username,
                Password
            };
            var authenticationDetails = new AWSCognito.AuthenticationDetails(authenticationData);

            cognitoUser.authenticateUser(authenticationDetails, {
                onSuccess: function (result) {
                    var accessToken = result.getAccessToken().getJwtToken();

                    /* Use the idToken for Logins Map when Federating User Pools with identity pools or when passing through an Authorization Header to an API Gateway Authorizer */
                    var idToken = result.idToken.jwtToken;
                    // console.log(accessToken)
                    // console.log(idToken)

                    // console.log("onSuccess: ", result);
                    resolve(result);
                },
                onFailure: function (err) {
                    // console.log(err)
                    alert(err);
                    reject(err);
                },
            });
        })
    }

    let history = useHistory();

    const logout = () => {

        const user = UserPool.getCurrentUser();
        if (user) {
            user.signOut();
            history.push("/");
        }
    };

    return (
        <AccountContext.Provider value={{ authenticate, getSession, logout }}>
            {props.children}
        </AccountContext.Provider>
    )

};

export { Account, AccountContext };

