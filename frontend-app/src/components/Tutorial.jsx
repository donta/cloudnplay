import React from "react";
import AuthNav from './Auth/AuthNav';
import '../css/tutorial.scss';

function Tutorial() {
  return (
    <div id="tutorial" className="container">
      <AuthNav />
      <h1 className="heading">Tutorial</h1>
      <div className="row">
        <div className="auth-div col col-lg-7 col-12">
          <section className="registration-section mb-5">
            <h1>Registration</h1>
            <p>Firstly, head over to our registration page to register an account.</p>
            <p>You will need our in-house administrators to validate your account before you are allowed to use our resources!</p>
            <p>Please be patient as we do have to verify your identity before granting you access to our resources! We will be sending you a confirmation email to notify that your account have been approved by our Administrators.</p>
          </section>
          <section className="login-section mb-5">
            <h1>Login</h1>
            <p>Sign in to your account once it is verified, you will be notified via the Email that you have been granted access to our Open Source Program</p>
          </section>
        </div>
        <div className="col col-lg-5 col-12">
          <div className="video-div ratio ratio-16x9">
            <iframe className="w-100" src="https://www.youtube.com/embed/SzPhx5GGay8" title="YouTube video player" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
          </div>
        </div>
      </div>
      <h1 className="mt-5 mb-4">How To Use</h1>
      <section className="developer-section mb-5">
        <h2>Developers</h2>
        <p>Please contact us via the contact us form if you wish to publish your game on our Cloud Platform for testing purposes.</p>
      </section>
      <section className="gamer-section mb-5">
        <h2>Gamers</h2>
        <p>Now, you will be greeted with a page filled with a deck of games. There are two options presented to you:</p>
        <ul className="p-2">
          <li>
            <h3>Nice DCV</h3>
            <p>You must have <a href="https://download.nice-dcv.com/"> Nice DCV Client </a> installed for our services to be delivered to you. You may install the required software by clicking on the hyperlink provided. After installation, just simply click on the DCV Button under the game of your choice to download the connection file and start playing immediately!</p>
            <p>Note: Please download the required client for your own operating system.</p>
          </li>
          <li>
            <h3>Parsec Client</h3>
            <p>You must have <a href="https://parsec.app/downloads/">Parsec Client</a> installed on your own PC. Next, click the Parsec button under the game of your choice. You will be redirected to a new tab which requires you to sign in to the selected Game server. Open up Parsec Client on the Game Server and sign in using your own account. This is for security purposes, but your login credentials will <b>NOT</b> be stored in our client after playing. Upon successful login, request a parsec connection from your PC's Parsec Client and approve the connection on the Game Server's Client.</p>
            <p>Note: Remember to approve both Keyboard and Mouse input!</p>
            <p>For more information, you may contact us via the contact form at the top right of your screen! :] or you may also watch the tutorial video at the top of the webpage!</p>
          </li>
        </ul>
      </section>
    </div>
  );
}

export default Tutorial;