import React from "react";
import AuthNav from './Auth/AuthNav';
import ContactDiv from './ContactDiv';

function ContactUs() {
  return (
    <div id="contact" className="container">
      <AuthNav/>
      <h1 className="heading">Contact Us</h1>
      <ContactDiv/>
    </div>
  );
}

export default ContactUs;