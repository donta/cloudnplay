import React, { useState } from 'react';
import SESCred from './Information/credentials'
let nodemailer = require("nodemailer");
let aws = require("@aws-sdk/client-ses");

const ContactForm = () => {

  const [status, setStatus] = useState("Submit");
  
  const handleSubmit = async (e) => {
    e.preventDefault();
    setStatus("Sending...");

    const { name, email, message } = e.target.elements;
    let details = {
      name: name.value,
      email: email.value,
      message: message.value,
    };
    const ses = new aws.SESClient({
      apiVersion: "2010-12-01",
      region: "ap-southeast-1",
      credentials: SESCred
    });
    
    // console.log(ses)
    // create Nodemailer SES transporter
    let transporter = nodemailer.createTransport({
      SES: { ses, aws },
    });
    
    // send some mail
    transporter.sendMail(
      {
        from: "cloudnplay123@gmail.com",
        to: "cloudnplay123@gmail.com",
        subject: `Feedback from ${details.name}`,
        text: `${details.message}
              My email address is ${details.email}`,
        ses: {
          // optional extra arguments for SendRawEmail
          Tags: [
            {
              Name: "tag_name",
              Value: "tag_value",
            },
          ],
        },
      },
      (err, info) => {
        // console.log(info);
        // console.log(err);
      }
    );
  
  
  };
  return (
    <div className="contact-card">
      <form className="form" onSubmit={handleSubmit}>
        <input type="text" id="name" className="form-input" placeholder="Name" required />
        <input type="email" id="email" className="form-input" placeholder="Email" required />
        <textarea type="text" id="message" className="form-input" placeholder="Message" required />
        <button id="submit-button" type="submit">{status}</button>
      </form>
    </div>
  );
};

export default ContactForm;