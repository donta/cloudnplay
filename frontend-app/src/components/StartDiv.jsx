import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';

const StartDiv = () => {
    return (
        <div id="logo-div" className="">
            <img src="images/logo_transparent.png" id="mainLogo" alt="logo"/>
            <div id="tagline">
                play your way, anytime, anywhere.
                <br></br>
                <br></br>
            </div>
        </div>
    )
};

export default StartDiv;