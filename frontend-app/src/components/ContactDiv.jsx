import React from 'react';
import ContactForm from './ContactForm';

const ContactDiv = () => {
    return (
        <div className="contact-div">
            <ContactForm />
            <img src={process.env.PUBLIC_URL + '/images/map.png'} alt="map" />
        </div>
    )
};

export default ContactDiv;