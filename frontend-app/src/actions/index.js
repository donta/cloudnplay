export const signup = (username) => {
    return {
        type: 'signup',
        payload: username 
    };
};