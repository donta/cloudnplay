# CloudNPlay

## Overview


Our application, CloudNPlay, is a web application that allows users to play graphically intensive games from their less powerful devices by leveraging on cloud computing resources on AWS. 

<!-- PROJECT LOGO -->
<br />

<!-- TABLE OF CONTENTS -->
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#ec2-configuration">EC2 Configuration</a></li>
        <li><a href="#game-streaming-configuration">Game Streaming Configuration</a></li>
      </ul>
    </li>
    <li>
      <a href="#using-the-app">Using the App</a>
      <ul>
        <li><a href="#user-guide">User Guide</a></li>
        <li><a href="#admin-guide">Admin Guide</a></li>
      </ul>
    </li>
    <li>
      <a href="#detailed-list-of-resources">Detailed list of resources</a>
    </li>
    <li>
      <a href="#versioning">Versioning</a>
    </li>
    <li>
      <a href="#acknowledgements">Acknowledgements</a>
      <ul>
        <li><a href="#contributors">Contributors</a></li>
        <li><a href="#resources">Resources</a></li>
      </ul>
    </li>  
  </ol>



<!-- ABOUT THE PROJECT -->
## About The Project

CloudNPlay is online at https://www.cloudnplay.com


### Built With

CloudNPlay is a [React.js](https://reactjs.org/) project bootstrapped with [`create-next-app`](https://github.com/facebook/create-react-app). It integrates AWS SDK for JavaScript to leverage on AWS resources and make use of its functionalities, and is deployed using AWS Amplify.


<!-- GETTING STARTED -->
## Getting Started

### Prerequisites

Ensure Node.js and NPM is installed. [https://nodejs.org/en/download/](https://nodejs.org/en/download/) 
* npm
  ```sh
  npm install npm@latest -g
  ```

1. Clone the repo or download the repository
  ```sh
  git clone https://github.com/CloudDevDOnDon/CloudGamingDev.git
  ```
   
2. Navigate to the main folder
  ```sh
  cd my-app
  ```
   

3. Install NPM packages
  ```sh
  npm install
  npm run build
  ```
  
4. Run the Application
  ```sh
  npm start
  ```

Steps to deploy the application on Amplify:

1. Deploy the github library on Amplify as a front-end application ( React Side ) by linking github.
2. Copy the Launch Template into the build settings.
3. Set up the Cognito authentication by creating a user pool. 
4. Set up 1 additional S3 Bucket, for DCV Configuration file. 
5. Import all lambda codes in our github library into AWS Lambda.
6. Create a staging EC2 Instance and configure the necessary dependencies, steps are inside the Configuration section.

##### Launch Template
<pre>
version: 1  
frontend:  
   phases:  
     preBuild:  
       commands:  
         - cd my-app  
         - npm install  
    build:  
      commands:  
        - npm run build  

  artifacts:  
    baseDirectory: ./my-app/build  
    files:  
      - '**/*'  
  cache:  
    paths:  
      - node_modules/**/*  
backend:  
  phases:  
    preBuild:  
      commands:  
        - update-alternatives --install /usr/bin/python3 python3 /usr/local/bin/python3.8 11  
        - /usr/local/bin/pip3.8 install --user pipenv  
        - amplifyPush --simple  
</pre>

### EC2 Configuration
Microsoft Windows Server 2019 Base AMI with g4dn.xlarge instance was the base image used which provides the NVIDIA Tesla T4 GPU with an Intel Xeon Cascade Lake Processor to power the games. 
<br/>
Since G4dn instances from AWS uses the Tesla T-series graphics processing unit, we are not able to configure the graphics drivers through the commercialised method, which is through Nvidia's Geforce Experience. Hence, we have to install the GRId drivers which are only available for AWS customers only. The following steps are applied:
1) Open powershell
2) We enter this command which allows us to directly install from Amazon's S3 Bucket of licensed drivers.
<pre>
$Bucket = "ec2-windows-nvidia-drivers"
$KeyPrefix = "latest"
$LocalPath = "$home\Desktop\NVIDIA"
$Objects = Get-S3Object -BucketName $Bucket -KeyPrefix $KeyPrefix -Region us-east-1
foreach ($Object in $Objects) {
    $LocalFileName = $Object.Key
    if ($LocalFileName -ne '' -and $Object.Size -ne 0) {
        $LocalFilePath = Join-Path $LocalPath $LocalFileName
        Copy-S3Object -BucketName $Bucket -Key $Object.Key -LocalFile $LocalFilePath -Region us-east-1
    }
</pre>
3) Navigate through the ZIP file and extract the Win-Server-2019 Nvidia Driver
4) Install the driver and reboot the instance, verify if graphics driver is successfully installed in device manager by running a game of our choice.

### Game Streaming Configuration

##### For Parsec:
1) Install Parsec Cloud Drivers into our instance through the following command.
<pre>
\[Net.ServicePointManager\]::SecurityProtocol = "tls12, tls11, tls" 
$ScriptWebArchive = "https://github.com/parsec-cloud/Parsec-Cloud-Preparation-Tool/archive/master.zip"  
$LocalArchivePath = "$ENV:UserProfile\Downloads\Parsec-Cloud-Preparation-Tool"  
(New-Object System.Net.WebClient).DownloadFile($ScriptWebArchive, "$LocalArchivePath.zip")  
Expand-Archive "$LocalArchivePath.zip" -DestinationPath $LocalArchivePath -Force  
CD $LocalArchivePath\Parsec-Cloud-Preparation-Tool-master\ | powershell.exe .\Loader.ps1  
</pre>
This include sound drivers, parsec input latency stabiliser, encoding and decoding optimisation for the cloud window server.

##### For NICE DCV:
1) Install the NICE DCV Server on Windows
Refer to the guide in [https://docs.aws.amazon.com/dcv/latest/adminguide/setting-up-installing-wininstall.html](https://docs.aws.amazon.com/dcv/latest/adminguide/setting-up-installing-wininstall.html)

2) Upgrade the NICE DCV Server on Windows
Refer to the guide in [https://docs.aws.amazon.com/dcv/latest/adminguide/setting-up-upgrading.html](https://docs.aws.amazon.com/dcv/latest/adminguide/setting-up-upgrading.html)

3) Enable the QUIC UDP transport protocol
By default, NICE DCV uses the WebSocket protocol, which is based on TCP, for data transport. You can configure NICE DCV to use the QUIC transport protocol, which is based on UDP, for data transport. Using QUIC can improve performance if your network experiences high latency and packet loss.
Refer to the guide in [https://docs.aws.amazon.com/dcv/latest/adminguide/enable-quic.html](https://docs.aws.amazon.com/dcv/latest/adminguide/enable-quic.html)

## Using the App

### User Guide
Refer to guide in Tutorial Section of CloudNPlay Website.

### Admin Guide
Manage user accounts using the AWS Cognito user pool's Users and groups.

## Detailed List of Resources
#### React:
React  
React Router DOM  
React BootStrap  
React Router  
Amazon Web Service Amplify-React  
Amazon-cognito-identity-js  

#### Front End Framework
BootStrap  
Amazon Web Service Amplify  

#### Back End Framework
Amazon Web Service Cognito  
Amazon Web Service Amplify  

#### Database Framework  
Amazon Web Service S3  

#### Deployment Framework  
Amazon Web Services  
NodeJS 

#### External Dependencies / API  
Amazon Web Services-SDK  
Parsec-SDK  
Parsec Client  
NICE DCV Client  


## Versioning

We use [Git](https://ourcodingclub.github.io/tutorials/git/) for versioning. For the versions available, see the [repository](https://github.com/CloudDevDOnDon/CloudGamingDev/tags). 


## Acknowledgements

### Contributors
* **Dr. Don Ta** - *Principal Investigator*
* **Yeo Yao Cong** - *Cloud Deployment / Quality Assurance* - [Yeo Yao Cong](https://github.com/izhcong1997)
* **Koh Shi Qi** - *Front-end Developer* - [Koh Shi Qi](https://github.com/shiqi926)
* **Low Qi long** - *Back-end Developer / Game-tester* - [Low Qi long](https://github.com/lowqilong)
* **Lee Shun Hui** - *Back-end Developer / Cost-Simulator* - [Lee Shun Hui](https://github.com/Shun-97)



### Resources
* [GitHub Emoji Cheat Sheet](https://www.webpagefx.com/tools/emoji-cheat-sheet)
* [TechGuru](https://www.youtube.com/channel/UCPmCidEAN9JrG1OwahlAkIQ)
* [Choose an Open Source License](https://choosealicense.com)
* [GitHub Pages](https://pages.github.com)
* [Programming With Mosh](https://www.youtube.com/watch?v=Ke90Tje7VS0&ab_channel=ProgrammingwithMosh)
* [freecodecamp](https://www.youtube.com/watch?v=Y0-qdp-XBJg&ab_channel=freeCodeCamp.org)
* [AWS](https://aws.amazon.com/)
* [Singapore Management University](https://scis.smu.edu.sg/)
* [ReactJS](https://reactjs.org/)
* [React-BootStrap](https://react-bootstrap.github.io/getting-started/introduction/)
* [othneildrew/Best-README-Template](https://github.com/othneildrew/Best-README-Template)

* Other than that, we did not plagiarize any code from our friends, seniors nor any online repository

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=for-the-badge
[contributors-url]: https://github.com/othneildrew/Best-README-Template/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=for-the-badge
[forks-url]: https://github.com/othneildrew/Best-README-Template/network/members
[stars-shield]: https://img.shields.io/github/stars/othneildrew/Best-README-Template.svg?style=for-the-badge
[stars-url]: https://github.com/othneildrew/Best-README-Template/stargazers
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=for-the-badge
[issues-url]: https://github.com/othneildrew/Best-README-Template/issues
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/othneildrew
[product-screenshot-nav]: my-app/public/images/nav.png
[product-screenshot-games]: my-app/public/images/games.png

